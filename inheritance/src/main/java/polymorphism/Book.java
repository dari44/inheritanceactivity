package polymorphism;

public class Book {
    protected String title;
    private String author;

    //constructor
    public Book(String title, String author){
        this.title = title;
        this.author = author;
    }

    //get methods
    public String getTitle(){
        return this.title;
    }

    public String getAuthor(){
        return this.author;
    }

    @Override
    public String toString(){
        return this.title + ", " + this.author;
    }
}
