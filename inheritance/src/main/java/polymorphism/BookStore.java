package polymorphism;
/**
 * Hello world!
 *
 */
public class BookStore
{
    public static void main( String[] args )
    {
        Book[] list = new Book[5];
        list[0] = new Book("Title1", "A");
        list[1] = new ElectronicBook("t2", "B", 10);
        list[2] = new Book("t3", "C");
        list[3] = new ElectronicBook("t4", "D", 30);
        list[4] = new ElectronicBook("t5", "E", 40);

        for (Book x : list){
            System.out.println(x);
        }

        //list[1].getNumberBytes();
        ElectronicBook eb = new ElectronicBook("x", "y", 1);
        System.out.println(eb.getNumberBytes());
        ElectronicBook b = (ElectronicBook)list[1];
        System.out.println(b.getNumberBytes());
        
        //error at runtime
        ElectronicBook c = (ElectronicBook)list[0];
        System.out.println(c.getNumberBytes());
    }   
}
