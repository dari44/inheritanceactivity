package polymorphism;

public class ElectronicBook extends Book{
    private int numberBytes;

    //constructor
    public ElectronicBook(String title, String author, int numberBytes){
        super(title,author);
        this.numberBytes = numberBytes;
    }

    //getters
    public int getNumberBytes(){
        return this.numberBytes;
    }

    @Override
    public String toString(){
        return super.toString() + ", " + this.numberBytes;
    }
}
